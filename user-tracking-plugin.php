
<?php
/*
Plugin Name: EH - User Tracking
Description: Track every move for your users
Author: Ebrahim Hendawy
*/


// Class File
require_once plugin_dir_path(__FILE__) . 'include/class.php';

// Admin Settings
add_action('init',function(){
$args = array(
	'title'        => 'User Tracking Settings',
	'prefix'       => 'eh_uk_',
	'action'       => 'theme_options',
	'redirect_url' => admin_url( 'admin.php?page=eh_usertracking' ),
	'setting_page' => array(
		'parent'      => true, //display as parent or child menu item
		'parent_slug' => '', //required if parent is set to false
		'capability'  => 'manage_options',
		'name'        => esc_html__( 'User Tracking', 'wsd' ),
		'slug'        => 'eh_usertracking',
		'icon'        => '', //required if parent is set to true
		'position'    => 10, //required if parent is set to true
	),
	'form_args'    => array(
		'id'           => 'posefwt',
		'class'        => 'perost',
		'nonce_action' => 'poerfst',
		'nonce_name'   => 'pwdfost',
    ),
    
	'input_args'   => array(
        array(
			'id'      => 'enablebox',
			'type'    => 'checkbox',
			'class'   => 'new_inpzut',
			'label'   => 'Activate Options',
			'desc'    => 'Check this box to enable Every Feature',
			'options' => array(
                'gtmbox'       => 'Google Tag Manager',
                'headbox'      => 'Head Codes',
                'footbox'      => 'Footer Codes',
			)
		),
		array(
			'id'          => 'gtmcode',
			'type'        => 'text',
			'class'       => 'new_input',
			'label'       => 'GTM Code',
			'desc'        => '*Please add full code like GTM-XXXXX',
			'placeholder' => 'GTM-XXXXX',
        ),
		array(
			'id'          => 'headcode',
			'type'        => 'textarea',
			'class'       => 'new_input',
			'label'       => 'Head Codes',
			'desc'        => 'This code add before tag /head',
			'placeholder' => '.............',
        ),
        array(
			'id'          => 'footcode',
			'type'        => 'textarea',
			'class'       => 'new_input',
			'label'       => 'Footer Codes',
			'desc'        => 'This code add before tag /body',
			'placeholder' => '.............',
		),
		
	)
);

$form_builder = new DW_Form_Builder( $args );
});

// CheckBox Setting
$data = get_option('eh_uk_enablebox');
foreach($data as $datum){
    if($datum == 'gtmbox'){
        // GTM Code in Header and Footer
        function GTNHEADCODE() {
            echo "
            <!-- Google Tag Manager EHUTracking -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','". get_option('eh_uk_gtmcode') ."');</script>
            <!-- End Google Tag Manager EHUTracking -->
            ";
        }      
        add_action( 'wp_head', 'GTNHEADCODE', 100 );
        
        function GTNFOOTCODE() {
            echo "
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src='https://www.googletagmanager.com/ns.html?id=". get_option('eh_uk_gtmcode') ."'
            height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            ";
        }
        add_action( 'wp_footer', 'GTNFOOTCODE' );
    }else{}
    if($datum == 'gtmbox'){
   
        // Header Code
        function hcode() {
            echo get_option('eh_uk_headcode');
        }
        add_action( 'wp_head', 'hcode' );
	}else{}
	if($datum == 'footbox'){

        // Footer Code
        function fcode() {
            echo get_option('eh_uk_footcode');
        }
        add_action( 'wp_footer', 'fcode' );
    }else{}
  };











